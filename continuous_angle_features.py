# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: ProteinNet_vep
#     language: python
#     name: proteinnet_vep
# ---

# +
import numpy as np
import pandas as pd
# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None

# Display up to 60 columns of a dataframe
pd.set_option('display.max_columns', 60)

import json
import requests
import math
import Bio.PDB as bp
pdbl = bp.PDBList()


import matplotlib.pyplot as plt
# %matplotlib inline

# Set default font size
plt.rcParams['font.size'] = 24

# Internal ipython tool for setting figure size
from IPython.core.pylabtools import figsize


import seaborn as sb
# Set default font size
sb.set(font_scale = 1.1)
custom_style = {'axes.labelcolor': 'black',
                'xtick.color': 'black',
                'ytick.color': 'black'}
sb.set_style("white", rc=custom_style)

# +
# the 4 atoms that define the CA-CB rotational angle
chi1_dict = dict(
        ARG=['N', 'CA', 'CB', 'CG'],
        ASN=['N', 'CA', 'CB', 'CG'],
        ASP=['N', 'CA', 'CB', 'CG'],
        CYS=['N', 'CA', 'CB', 'SG'],
        GLN=['N', 'CA', 'CB', 'CG'],
        GLU=['N', 'CA', 'CB', 'CG'],
        HIS=['N', 'CA', 'CB', 'CG'],
        ILE=['N', 'CA', 'CB', 'CG1'],
        LEU=['N', 'CA', 'CB', 'CG'],
        LYS=['N', 'CA', 'CB', 'CG'],
        MET=['N', 'CA', 'CB', 'CG'],
        PHE=['N', 'CA', 'CB', 'CG'],
        PRO=['N', 'CA', 'CB', 'CG'],
        SER=['N', 'CA', 'CB', 'OG'],
        THR=['N', 'CA', 'CB', 'OG1'],
        TRP=['N', 'CA', 'CB', 'CG'],
        TYR=['N', 'CA', 'CB', 'CG'],
        VAL=['N', 'CA', 'CB', 'CG1'],
    )

# from: https://stackoverflow.com/questions/20305272/\
#dihedral-torsion-angle-from-four-points-in-cartesian-coordinates-in-python
def calc_dihedral(p):
    b = p[:-1] - p[1:]
    b[0] *= -1
    v = np.array( [ v - (v.dot(b[1])/b[1].dot(b[1])) * b[1] for v in [b[0], b[2]] ] )
    # Normalize vectors
    v /= np.sqrt(np.einsum('...i,...i', v, v)).reshape(-1,1)
    b1 = b[1] / np.linalg.norm(b[1])
    x = np.dot(v[0], v[1])
    m = np.cross(v[0], b1)
    y = np.dot(m, v[1])
    return np.degrees(np.arctan2( y, x ))

def url_response(url):
    r = requests.get(url=url)
    if r.status_code == 200:
        json_result = r.json()
        return json_result
    else:
        return None

id_fields = ["pdb_id",
            "entity_id",
            "chain_id",
            "model_id"]

info_fields = ["rama",
          "residue_name",
          "residue_number",
          "author_residue_number",
          "phi", 
          "psi"]

# get phi, psi and the rama / molprobity validation flag from PDBe
def get_rama_sidechain_listing(pdb_id):
    rama_search_url = "https://www.ebi.ac.uk/pdbe/api/validation/rama_sidechain_listing/entry/" + pdb_id 
    # internal EBI VM url:
    #rama_search_url = "http://ves-oy-89.ebi.ac.uk/pdbe/api/\
    #validation/rama_sidechain_listing/entry/" + pdb_id
    output = []
    results = url_response(rama_search_url)
    for entity in results[pdb_id.lower()]["molecules"]:
        entity_id = entity.get('entity_id')
        for chain in entity.get("chains"):
            chain_id = chain.get("chain_id")
            for model in chain.get("models"):
                model_id = model.get("model_id")
                id_out = [pdb_id, entity_id, chain_id, model_id]
                for residue in model["residues"]:
                    output.append(id_out + [residue[f] for f in info_fields])
    df = pd.DataFrame(output, columns=id_fields + info_fields)
    df[["residue_number",
        "author_residue_number"]] = df[[ "residue_number",
                                        "author_residue_number"]].astype(int)
    return df
    
def get_chi_1(pdb_id):
    output = []
    # download ad parse structure
    pdbl.retrieve_pdb_file(pdb_id, pdir="./")
    mmcif_dict = bp.MMCIF2Dict.MMCIF2Dict(pdb_id + '.cif')
    d = {'group_PDB' : mmcif_dict.get("_atom_site.group_PDB"), 
        'auth_ch_id' : mmcif_dict.get("_atom_site.label_asym_id"), 
         'author_residue_number' : mmcif_dict.get("_atom_site.auth_seq_id"), 
         'pdb_ch_id' : mmcif_dict.get("_atom_site.label_asym_id"), 
         'residue_number' : mmcif_dict.get("_atom_site.label_seq_id"),
         'residue_name' : mmcif_dict.get("_atom_site.label_comp_id"),
         'atom_id' : mmcif_dict.get("_atom_site.label_atom_id"),
         'Cartn_x' :  mmcif_dict.get("_atom_site.Cartn_x"),
         'Cartn_y' : mmcif_dict.get("_atom_site.Cartn_y"),
         'Cartn_z' : mmcif_dict.get("_atom_site.Cartn_z")}
    atoms_df = pd.DataFrame(d)

    # filter out heteroatoms and non-standard AA
    # this is a harsh filter, could use IUPAC ambiguities
    atoms_df = atoms_df[(atoms_df["group_PDB"] == "ATOM") &\
                        (atoms_df["residue_name"].isin(chi1_dict.keys()))]
    atoms_df[["Cartn_x",
              "Cartn_y",
              "Cartn_z"]] = atoms_df[["Cartn_x",
                                      "Cartn_y",
                                      "Cartn_z"]].astype(float)
    # group atoms by residue
    grouped_atoms_df = atoms_df.groupby("residue_number")
    for residue_number, group in grouped_atoms_df:
        residue_name = group["residue_name"].values[0]
        residue_number = group["residue_number"].values[0]
        author_residue_number = group["author_residue_number"].values[0]
        chain_id = group["pdb_ch_id"].values[0]
        
        # get coords for the 4 atoms defining the chi_1 dihedral angle
        chi_1_atoms = chi1_dict[residue_name]
        # compute angle value
        chi_1_atoms_coords = group[group["atom_id"].isin(chi_1_atoms)][["Cartn_x",
                                                                          "Cartn_y",
                                                              "Cartn_z"]].values

        # TODO: tidy up exception
        try:
            chi_1_angle = calc_dihedral(chi_1_atoms_coords)
        except:
            chi_1_angle = np.nan
            
        output.append([chain_id,
                       residue_name,
                       residue_number, 
                       author_residue_number,
                       chi_1_atoms,
                       chi_1_angle])
        
    df = pd.DataFrame(output, columns=["chain_id",
                       "residue_name",
                       "residue_number", 
                       "author_residue_number",
                       "chi_1_atoms",
                       "chi_1_angle"])
    df[["residue_number",
        "author_residue_number"]] = df[[ "residue_number",
                                        "author_residue_number"]].astype(int)
    return df



# -

df1 = get_rama_sidechain_listing("1cbs")
df2 = get_chi_1("1cbs")
df3 = pd.merge(df1, df2, on=["chain_id",
                             "residue_number",
                             "author_residue_number"] )
df3.head(30)
